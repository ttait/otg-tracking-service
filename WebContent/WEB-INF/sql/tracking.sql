
DROP TABLE IF EXISTS otg_tracking;
CREATE TABLE `otg_tracking` (
  `tracking_id` BIGINT NOT NULL AUTO_INCREMENT,
  `session_id` BIGINT NOT NULL,
  `device_id` varchar(25) NOT NULL DEFAULT '',
  `menu_id` int(11) NOT NULL,
  `tracking_action` varchar(20) NOT NULL DEFAULT '',
  `tracking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tracking_time`,tracking_id),
  KEY `session_id` (`session_id`,`tracking_time`),
  KEY `tracking_id` (`tracking_id`),
  KEY `tracking_dev_id` (device_id, tracking_time)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 collate utf8_unicode_ci;

DROP TABLE IF EXISTS otg_tracking_detail;
CREATE TABLE `otg_tracking_detail` (
  `tracking_id` bigint(11) NOT NULL,
  `tracking_action` varchar(20) NOT NULL,
  `tracking_key` varchar(20) NOT NULL,
  `tracking_detail` varchar(100) NOT NULL,
  `tracking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tracking_id`,`tracking_action`,`tracking_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 collate utf8_unicode_ci;

DROP PROCEDURE IF EXISTS sp_tracking_get_session;
DELIMITER ;;
CREATE PROCEDURE sp_tracking_get_session(DEVID varchar(20), MENUID INT, OUT SESSIONID LONG)
BEGIN
	DECLARE TTIME TIMESTAMP; #	holder for the time of last track
	DECLARE TACTION VARCHAR(20); #	holder for the last action

	DECLARE TIMEOUT TIMESTAMP; #	time to compare the last actions time to
	DECLARE TIMEOUT_INTERVAL INT; #	from the db, the interval after which we assume the transaction is closed
	DECLARE SESSIONACTIVE TINYINT; # flag to see if the session is still active
	
	SET TIMEOUT_INTERVAL = (SELECT param_value FROM otg_app_config WHERE param_name="TRACKING_TIMEOUT_INTERVAL");
	SET TIMEOUT = (NOW() - INTERVAL TIMEOUT_INTERVAL MINUTE);
	SET SESSIONACTIVE = 0;

###	get the last record for this device from the db
SELECT session_id, tracking_action, tracking_time
INTO SESSIONID, TACTION, TTIME
FROM otg_tracking otg
WHERE otg.device_id = DEVID
AND tracking_time = (SELECT max(tracking_time) FROM otg_tracking WHERE device_id = DEVID)
LIMIT 1
COLLATE utf8_unicode_ci;

###	if the action is not null (meaning this is the first of the devices to get a session) or the action was close session, skip this part
IF (TACTION IS NOT NULL AND TACTION <> "CLOSE_SESSION") THEN
#	if the last action was not close session, check to see if the session is still valid
	IF (TTIME < TIMEOUT) THEN
		###	if the session has expired, close it in the DB
		INSERT INTO `otg_tracking` (`session_id`, `device_id`, `menu_id`, `tracking_action`)
		VALUES 	( SESSIONID, DEVID, 0, 'CLOSE_SESSION');
	ELSE
		###	set the flag to recognize that the session is active
		SET SESSIONACTIVE = 1;
	END IF;
END IF;

###	if the session is still active, use that id, else get the new one in a transaction, so it wont get clobbered, accidentally
IF (SESSIONACTIVE <> 1) THEN
	START TRANSACTION;
		# get next sessionID
		SET SESSIONID = (SELECT (MAX(`session_id`)) FROM otg_tracking) + 1;
		IF (SESSIONID IS NULL) THEN
			SET SESSIONID = 1;
		END IF;
		INSERT INTO `otg_tracking` (`session_id`, `device_id`, `menu_id`, `tracking_action`)
		VALUES ( SESSIONID, DEVID, MENUID, 'OPEN_SESSION');
	COMMIT;
END IF;

END;;
DELIMITER ;

CREATE TABLE IF NOT EXISTS `otg_tracking_session` (
  `device_id` varchar(25) NOT NULL DEFAULT '',
  `session_id` bigint(20) NOT NULL,
  `tracking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `session_status` tinyint NOT NULL DEFAULT 0,
  PRIMARY KEY (`device_id`,`tracking_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE TABLE otg_tracking_session;

### insert data into new table for session
insert into otg_tracking_session 
select  device_id,session_id, max(tracking_time) as tracking_time, 0
from otg_tracking group by device_id;
### end data insert

DROP PROCEDURE IF EXISTS sp_tracking_get_session_new;
DELIMITER ;;
CREATE PROCEDURE sp_tracking_get_session_new(DEVID varchar(20), MENUID INT, OUT SESSIONID LONG)
BEGIN
	####################
	##	6/13/2014 - timothy tait
	## new procedure to get the tracking session, that doesnt lock the tracking table

## get variables
	SET @TIMEOUT_INTERVAL = (SELECT param_value FROM otg_app_config WHERE param_name="TRACKING_TIMEOUT_INTERVAL");
	SET @TIMEOUT = (NOW() - INTERVAL @TIMEOUT_INTERVAL MINUTE);
	SET @SESSIONACTIVE = 0;
## get active sessionID from session table, by device id, that is less than timout_interval
	SELECT device_id, session_id, tracking_time, session_status 
	INTO @deviceId, @sessId, @trackingTime, @status 
	FROM otg_tracking_session 
	WHERE device_id = DEVID;

	#	if device_id is not in table, add it
	IF @deviceId IS NULL THEN
		INSERT INTO otg_tracking_session (device_id) VALUES (DEVID);
		SET @deviceId = DEVID;
	END IF;
	#SELECT @deviceId, @sessId, @trackingTime, @status,@DEVID;
	
	#	check to see if the session is open and not timed out
	IF @status = 1 AND @trackingTime > @TIMEOUT THEN
		SET SESSIONID = @sessId;
	ELSE
		START TRANSACTION;
			# get next sessionID
			SET @sessId = (SELECT (MAX(`session_id`)) FROM otg_tracking_session) + 1;
			IF (@sessId IS NULL) THEN
				SET @sessId = 1;
			END IF;
			#	add new record to tracking_table
			INSERT INTO `otg_tracking` (`session_id`, `device_id`, `menu_id`, `tracking_action`)
			VALUES ( @sessId, @deviceId, MENUID, 'OPEN_SESSION');
			
			#	update session table with new session_id, and status as 1
			UPDATE `otg_tracking_session` 
			SET `session_id` = @sessId, session_status = 1, tracking_time = NOW()
			WHERE `device_id` = @deviceId ;
			
			#	set out variable
			SET SESSIONID = @sessId;
		COMMIT;
			
	END IF;
END;;
DELIMITER ;

DROP PROCEDURE if exists `sp_tracking_insert_action`;
DELIMITER ;;
CREATE  PROCEDURE `sp_tracking_insert_action`(DEVID VARCHAR(50), ACT VARCHAR(20))
BEGIN
	DECLARE MENUID INT; 
	DECLARE SESSIONID LONG;
	# get the session id for the item
	
	SET @track = (select param_value from otg_app_config WHERE param_name = "USE_TRACKING");
	IF @track = "1" THEN 
		CALL sp_get_current_menu_out(DEVID, MENUID);
		
		# get the session id for the item
		CALL sp_tracking_get_session_new(DEVID, MENUID,SESSIONID);
		if sessionid is null then
			select "FAIL!";
		END IF;
		
		INSERT INTO `otg_tracking` (`session_id`, `device_id`, `menu_id`, `tracking_action`)
		VALUES 	( SESSIONID, DEVID, MENUID, ACT);
		
		IF ACT = "CLOSE_SESSION" THEN
			UPDATE otg_tracking_session 
			SET session_status = 0 
			WHERE device_id = DEVID 
			AND session_id = SESSIONID;
		ELSE 
			UPDATE otg_tracking_session 
			SET tracking_time = NOW() 
			WHERE device_id = DEVID 
			AND session_id = SESSIONID;
		END IF;
		
		SELECT LAST_INSERT_ID() as tracking_id,SESSIONID as session_id;
	ELSE
		SELECT 0 as tracking_id,0 as session_id;
	END IF;
END;;
DELIMITER ;

DROP PROCEDURE if exists `sp_tracking_insert_details`;
DELIMITER ;;
CREATE PROCEDURE `sp_tracking_insert_details`(TRACKINGID LONG, ACT VARCHAR(20), TKEY VARCHAR(20),DETAIL VARCHAR(100))
BEGIN
	SET @track = (select param_value from otg_app_config WHERE param_name = "USE_TRACKING");
	IF @track = "1" THEN 
		INSERT INTO `otg_tracking_detail` (`tracking_id`, `tracking_action`, `tracking_key`,`tracking_detail`)
		VALUES 	( TRACKINGID, ACT, TKEY, DETAIL);
	END IF;
END;;
DELIMITER ;
			
CREATE TABLE IF NOT EXISTS  `otg_tracking_history` (
  `tracking_id` bigint(20) NOT NULL,
  `session_id` bigint(20) NOT NULL,
  `device_id` varchar(25) NOT NULL DEFAULT '',
  `menu_id` int(11) NOT NULL,
  `tracking_action` varchar(20) NOT NULL DEFAULT '',
  `tracking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tracking_time`,`tracking_id`),
  KEY `session_id` (`session_id`,`tracking_time`),
  KEY `tracking_id` (`tracking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `otg_tracking_detail_history` (
  `tracking_id` bigint(11) NOT NULL,
  `tracking_action` varchar(20) NOT NULL,
  `tracking_key` varchar(20) NOT NULL,
  `tracking_detail` varchar(100) NOT NULL,
  `tracking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tracking_id`,`tracking_action`,`tracking_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `otg_app_config` (`param_name`, `param_value`, `last_upd_datetime`, `last_upd_user`, `create_datetime`, `create_user`)
VALUES
	( 'TRACKING_TIMEOUT_INTERVAL', '15', NOW(), 'system', NOW(), 'system'),
	( 'TRACKING_OFFLOAD_INTERVAL', '14', NOW(), 'system', NOW(), 'system'),
	( 'USE_TRACKING','1',NOW(),'system', NOW(), 'system');


DROP PROCEDURE IF EXISTS sp_tracking_admin_offload_data;
DELIMITER ;;
CREATE PROCEDURE sp_tracking_admin_offload_data()
BEGIN
	SET @interval = (SELECT param_value FROM otg_app_config WHERE param_name = "TRACKING_OFFLOAD_INTERVAL");
	IF @interval IS NULL THEN
		SET @interval = 14;
	END IF;
	set @STARTDATE = CURDATE() - INTERVAL @interval DAY;

	INSERT INTO otg_tracking_history
	SELECT *
	FROM otg_tracking
	WHERE tracking_time < @STARTDATE;
		
	INSERT INTO otg_tracking_detail_history
	SELECT *
	FROM otg_tracking_detail
	WHERE tracking_time < @STARTDATE;
		
	DELETE FROM otg_tracking
	WHERE tracking_time < @STARTDATE;
		
	DELETE FROM otg_tracking_detail
	WHERE tracking_time < @STARTDATE;

END;;
DELIMITER ;


CREATE TABLE `error_log` (
  `device_id` varchar(25) NOT NULL DEFAULT '',
  `venue_id` int(11) NOT NULL,
  `error_text` TEXT NOT NULL DEFAULT '',
  `error_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`error_time`,`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

DROP PROCEDURE IF EXISTS sp_insert_error_text;
DELIMITER ;;
CREATE PROCEDURE sp_insert_error_text(UID VARCHAR(20), ERRORTXT TEXT)
BEGIN
	SET @venueId = (SELECT restaurant_id 
	FROM devices d
	JOIN devices_restaurants dr ON dr.device_id = d.id
	WHERE d.uid = UID);
	
	INSERT INTO `error_log` (`device_id`, `venue_id`, `error_text`)
	VALUES
	(UID, @venueId, ERRORTXT);
 
 END;;
 DELIMITER ;

			