package com.services.otg.utils;

import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

public class CustomJacksonProvider extends JacksonJaxbJsonProvider
{
	public CustomJacksonProvider()
	{

	ObjectMapper mapper = new ObjectMapper();
	mapper.configure(org.codehaus.jackson.map.DeserializationConfig.Feature.UNWRAP_ROOT_VALUE,false);
	mapper.configure(
			org.codehaus.jackson.map.SerializationConfig.Feature.WRAP_ROOT_VALUE,
			false);
	mapper.getSerializationConfig().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
	_mapperConfig.setMapper(mapper);
	_mapperConfig.getConfiguredMapper().setAnnotationIntrospector(
			new JaxbAnnotationIntrospector());
	
	}
}