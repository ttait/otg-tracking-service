package com.services.otg.utils;

public class TrackingConstants {
	private final String pickFlight = "FLIGHT_PICK";
	private final String skipFlight = "FLIGHT_SKIP";
	private final String pickLanguage = "LANGUAGE_PICK";
	private final String backFlight = "FLIGHT_BACK";
	private final String pickTab = "TAB_PICK";
	private final String homeCategory = "CATEGORY_HOME";
	private final String listCategory = "CATEGORY_LIST";
	private final String addItem = "ITEM_ADD";
	private final String addUpsell = "ITEM_ADD_UPSELL";
	private final String addCaroUsel = "ITEM_ADD_CAROUSEL";
	private final String addLast = "ITEM_ADD_LAST";
	private final String addList = "ITEM_ADD_LIST";
	private final String addFull = "ITEM_ADD_FULL";
	private final String submitOrder = "ORDER_SUBMIT";
	private final String changeQty = "QTY_CHANGE";
}
