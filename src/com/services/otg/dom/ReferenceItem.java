package com.services.otg.dom;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReferenceItem {
	protected String id;
	protected String name;
	public String getId() {
		return id;
	}
	public void setId(int id) {
		this.id = ""+id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
