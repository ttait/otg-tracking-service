package com.services.otg.dom;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OtgObject {
	private boolean success;
	private String failureReason;
	
	public OtgObject() {
		this.success = true;
	}
	public OtgObject(String error) {
		this.success = false;
		this.failureReason = error;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getFailureReason() {
		return failureReason;
	}
	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}
	

}
