package com.services.otg.db;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class AbstractTemplate {
	@Autowired
	protected DataSource dataSource;
	
	protected JdbcTemplate jdbc;

   public void setDataSource(DataSource dataSource) {
	      this.dataSource = dataSource;
	      //this.cloneJDBCTemplate = new JdbcTemplate(dataSource);
	   }
   protected void getTemplate() {
	   if (jdbc == null)
		   jdbc = new JdbcTemplate(dataSource);
   }

}
