package com.services.otg.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.collections.map.MultiValueMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.services.otg.dom.OtgObject;

public class TrackingTemplate extends AbstractTemplate {
	
	public OtgObject insertTrackingAction(String devId, String action, Map<String,String> detail) {
		getTemplate();
		String sql = "{call sp_tracking_insert_action(?,?)}";		
		
		SqlRowSet rs = jdbc.queryForRowSet(sql, devId, action);
		long sessionId = 0;
		long trackingId = 0;
		while (rs.next()) {
			sessionId = rs.getLong("session_id");
			trackingId = rs.getLong("tracking_id");
		}
		String resp = "";
		if ( null!= detail && !detail.isEmpty()) {
			for (Iterator it = detail.entrySet().iterator(); it.hasNext(); ) {
				Map.Entry pairs = (Map.Entry)it.next();
				//pairs.getKey() + " = " + pairs.getValue();
				String success= insertTrackingDetail(trackingId, action, pairs.getKey().toString(),pairs.getValue().toString());
				if (null != success) resp+= success;
				it.remove();
			
			}
		}
		if (resp.length() > 1 ) return new OtgObject(resp);
		return new OtgObject();
	}
	
	private String insertTrackingDetail(long trackingId, String action, String key,String detail) {
		
		String sql = "{call sp_tracking_insert_details("+trackingId+",'"+action+"','"+key+"','"+detail+"')}";		
		try {
			jdbc.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		
		return null;
	}
	public OtgObject insertTrackingAction(MultivaluedMap<String,String> req) {
		List<String> jsonArray = new ArrayList<String>(req.keySet());
		StringBuilder json = new StringBuilder();
		for (int i = 0; i < jsonArray.size(); i++ ) {
			String k = jsonArray.get(i);
			System.out.print("\njson["+i+"] key: " + k);
			json.append(k);
			if (k.indexOf('}') < 1) {
				String v = req.getFirst(k);
				System.out.print("json["+i+"] value: " + v);
				json.append("=" + v);
			}
		}
		
		System.out.println("\n----- replaced json: "+ json.toString());
		String devId = null;
		String action = null;
		HashMap<String,String> details = new HashMap<String, String>();
		try{		  
			JSONParser p = new JSONParser();
			Object obj = p.parse(json.toString());
			 
			JSONObject jsonObject = (JSONObject) obj;
	 
			devId = (String) jsonObject.get("deviceId");
			action = (String) jsonObject.get("action");
			Iterator<String> iterator = new ArrayList<String>(jsonObject.keySet()).iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				if ("deviceId".equalsIgnoreCase(key) || "action".equalsIgnoreCase(key)) {
					continue;
				}
				details.put(key, jsonObject.get(key).toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new OtgObject(e.getLocalizedMessage());
		}
		
		return insertTrackingAction(devId, action, details);
	}
	public OtgObject insertTrackingError(MultivaluedMap<String,String> req) {
		getTemplate();
		System.out.println("Tracking Error: " + req);
		Map<String,String> m = getJsonForError(req);
		
		String devId = m.get("deviceId");
		String errorTxt = m.get("errorTxt");
		
		String sql = "{CALL sp_insert_error_text('"+devId + "','"+ errorTxt +"')}";
		try {
			jdbc.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
			return new OtgObject(e.getMessage());
		}

		return new OtgObject();

	}
	public OtgObject insertTrackingError(String devId, String errorTxt) {
		getTemplate();
		System.out.println("---- form params: "+devId+":"+errorTxt);
		String sql = "{sp_insert_error_text("+devId + ","+ errorTxt +")}";
		try {
			jdbc.execute(sql);
		} catch (Exception e) {
			e.printStackTrace();
			return new OtgObject(e.getMessage());
		}

		return new OtgObject();

	}

	private Map<String,String> getJsonFromMap(MultivaluedMap<String,String> req) {
		System.out.println("\n----- in getJsonFromMap");
		List<String> jsonArray = new ArrayList<String>(req.keySet());
		System.out.println("\n----- array: " + jsonArray.toString());
		StringBuilder json = new StringBuilder();
		HashMap<String,String> map = new HashMap<String, String>();
		for (int i = 0; i < jsonArray.size(); i++ ) {
			String key = jsonArray.get(i);
			//System.out.print("\njson["+i+"] key: " + k);
			//json.append(k);
			if (key.indexOf('}') < 1) {
				String value = req.getFirst(key);
				//System.out.print("json["+i+"] value: " + v);
				//json.append("=" + v);
				map.put(key, value);
				
			}
		}
		return map;
	}
	private Map<String,String> getJsonForError(MultivaluedMap<String,String> req) {
		List<String> jsonArray = new ArrayList<String>(req.keySet());
		StringBuilder json = new StringBuilder();
		for (int i = 0; i < jsonArray.size(); i++ ) {
			String k = jsonArray.get(i);
			System.out.print("\njson["+i+"] key: " + k);
			json.append(k);
			if (k.indexOf('}') < 1) {
				String v = req.getFirst(k);
				System.out.print("json["+i+"] value: " + v);
				json.append("=" + v);
			}
		}
		HashMap<String,String> details = new HashMap<String, String>();
		try{		  
			JSONParser p = new JSONParser();
			Object obj = p.parse(json.toString());
			 
			JSONObject jsonObject = (JSONObject) obj;
	 
			String devId = (String) jsonObject.get("deviceId");
			String errorText = (String) jsonObject.get("errorTxt");
			details.put("deviceId", devId);
			details.put("errorTxt", errorText);
		} catch (Exception e) {
			e.printStackTrace();
			return new HashMap<String,String>();
		}

		return details;
	}
}
