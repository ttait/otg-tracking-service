package com.services.otg.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.PathParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;

import com.services.otg.db.TrackingTemplate;
import com.services.otg.dom.OtgObject;
import com.services.otg.dom.ReferenceItem;
import com.services.otg.interfaces.TrackingService;

public class TrackingServiceImpl implements TrackingService {

	@Autowired
	private TrackingTemplate trackingTemplate;

	private ReferenceItem version;

	public OtgObject test() {

		OtgObject o = new OtgObject();
		o.setFailureReason("testing successful");
		return o;
	}
	
	public OtgObject trackAction(String deviceId, String action, String view) {
		String insertAction = action;
		String detail = view;
		HashMap<String,String> details = new HashMap<String,String>();
		//	special case for flight
		if ("flight".equalsIgnoreCase(action)) {
			if (!("skip".equalsIgnoreCase(view) || "back".equalsIgnoreCase(view))) {
				details.put("flight_id", view);
			}
			else insertAction = action + "_" + view;
		} else if ("lang".equalsIgnoreCase(action)) {
			details.put("language", view);
		} else {
			insertAction = action + "_" + view;
		}
		//if (detail != null) details.add(detail);
		return trackingTemplate.insertTrackingAction(deviceId, insertAction, details);
	}

	public OtgObject trackAction(String deviceId, String action) {

		if ("session-reset".equalsIgnoreCase(action)) {
			action = "CLOSE_SESSION";
		} 
		return trackingTemplate.insertTrackingAction(deviceId, action, null);
	}

	public OtgObject trackAction(String deviceId, String action, String view, String payload) {

		OtgObject o = new OtgObject();
		o.setFailureReason("track Action post-style: " + action +"--"+view+"--"+deviceId+"--"+payload);
		return o;
	}
	
	public OtgObject trackAction(MultivaluedMap<String, String> parameters) {
		
		return trackingTemplate.insertTrackingAction(parameters);
		
	}
	
	public OtgObject trackError(MultivaluedMap<String, String> parameters) {
		System.out.println("in trackError mv map.");
		return trackingTemplate.insertTrackingError(parameters);
		
	}
/*	
	public OtgObject trackError(TrackingErrorText tr) {
		System.out.println("in trackError object");
		return trackingTemplate.insertTrackingError(tr.getDeviceId(),tr.getErrorTxt());
		
	}
*/
	public OtgObject trackError(String devId,String errorTxt) {
		System.out.println("in trackError form params.");
		return trackingTemplate.insertTrackingError(devId,errorTxt);
	}

	public ReferenceItem getVersion() {
		if (null != version) return version;
		version = new ReferenceItem();
        Properties props = new Properties();
        try {
        	InputStream in = this.getClass().getClassLoader().getResourceAsStream("../otg-tracking-service.properties");
	        props.load(in);  


        	String propValue = props.getProperty("version");
        	version.setName("version");
        	version.setId(propValue);
		   
        } catch (Exception e) {
        	e.printStackTrace();
        }
	   return version;
   }



}
