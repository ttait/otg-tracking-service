package com.services.otg.interfaces;

import java.sql.Date;
import java.sql.Time;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;

import com.services.otg.dom.OtgObject;
import com.services.otg.dom.ReferenceItem;


@WebService(targetNamespace = "http://impl.otg.services.com/", portName = "TrackingServiceImplPort", serviceName = "TrackingServiceImplService")
@Path("/tracking")
public interface TrackingService {

	@GET
	@Path("/test")
    @Produces("application/json")
	public OtgObject test();
	
	@GET
	@Path("/trackAction/{deviceId}/{action}/{view}")
    @Produces("application/json")
	public OtgObject trackAction(@PathParam("deviceId") String deviceId,@PathParam("action") String action, @PathParam("view") String view);

	@GET
	@Path("/trackAction/{deviceId}/{action}")
    @Produces("application/json")
	public OtgObject trackAction(@PathParam("deviceId") String deviceId,@PathParam("action") String action);


	@POST
	@Path("/trackAction/test")
	@Consumes({"application/json","application/x-www-form-urlencoded"})
    @Produces("application/json")
	public OtgObject trackAction(@FormParam("deviceId") String deviceId,@FormParam("action") String action, @FormParam("view") String view, @FormParam("payload") String payload) ;

	@POST
	@Path("/trackAction/")
	@Consumes({"application/json","application/x-www-form-urlencoded"})
    @Produces("application/json")
	public OtgObject trackAction(MultivaluedMap<String, String> parameters) ;


	@POST
	@Path("/trackError/")
	@Consumes({"application/json","application/x-www-form-urlencoded"})
    @Produces("application/json")
	public OtgObject trackError(MultivaluedMap<String, String> parameters) ;

/*	@POST
	@Path("/trackError/")
	@Consumes({"application/json","application/x-www-form-urlencoded"})
    @Produces("application/json")
	public OtgObject trackError(@FormParam("deviceId") String devId,
								@FormParam("errorTxt") String errorTxt) ;
	@POST
	@Path("/trackError/")
	@Consumes({"application/json"})
    @Produces("application/json")
	public OtgObject trackError(TrackingErrorText tr) ;
*/

	@GET
	@Path("/getAppVersion/")
    @Produces("application/json")
	public ReferenceItem getVersion();

}
